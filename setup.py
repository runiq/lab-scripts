from distutils.core import setup


setup(name='labscripts',
      version='0.0.1',
      description='Assorted lab scripts',
      author='Patrice Peterson',
      author_email='patrice.peterson@itz.uni-halle.de',
      url='http://bitbucket.org/runiq/lab-scripts',
      py_modules=['labscripts.mkaln', 'labscripts.parserampage'],
      )
