from StringIO import StringIO
from textwrap import dedent

import Bio.SeqIO as SeqIO
import pytest

import labscripts.mkaln as mkaln


def make_fasta_fh(fasta, aln=False):
    func = SeqIO.parse if aln else SeqIO.read
    return func(StringIO(dedent(fasta)), 'fasta')


@pytest.fixture
def my_seq_record():
    fasta = """
    >target
    ABCDEFGHIJ
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_seq_record_with_gaps():
    fasta = """
    >target
    AB-C-DEFGHIJ
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_seq_record_with_gaps1():
    fasta = """
    >target
    --AB-C-DEFGHIJ
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_seq_record_with_gaps2():
    fasta = """
    >target
    AB-C-DEFGHI-J---
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_seq_record_target():
    fasta = """
    >template 11
    WXYZ
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_seq_record_target_with_gaps():
    fasta = """
    >template 11
    W-X-Y-Z
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_seq_record_target_with_multiple_gaps():
    fasta = """
    >template 11
    W--X-Y---Z
    """
    return mkaln.MySeqRecord(make_fasta_fh(fasta))


@pytest.fixture
def my_aln_full_target(my_seq_record):
    return mkaln.MyAln([my_seq_record])


@pytest.fixture
def my_aln_nogaps():
    fasta = """
    >target 2
    BCDE

    >template1 11
    WXYZ
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


@pytest.fixture
def my_aln_template_gap_at_1():
    fasta = """
    >target 2
    BCDE

    >template1 11
    W-XY
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


@pytest.fixture
def my_aln_template_2_gaps_at_1():
    fasta = """
    >target 2
    BCDE

    >template1 11
    W--X
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


@pytest.fixture
def my_aln_full_range():
    fasta = """
    >target 1
    ABCDEFGHIJ

    >template1 11
    W---X-Y--Z
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


@pytest.fixture
def my_aln_1_gap_at_start():
    fasta = """
    >target 2
    BCDEFGHIJ

    >template1 11
    W--X-Y--Z
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


@pytest.fixture
def my_aln_1_gap_at_end():
    fasta = """
    >target 1
    ABCDEFGHI

    >template1 11
    W--X-Y--Z
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


@pytest.fixture
def my_aln_simple():
    fasta = """
    >target 2
    BC-D

    >template1 11
    WXYZ
    """
    return mkaln.MyAln(make_fasta_fh(fasta, aln=True))


class TestMySeqRecord:
    def test_init(self, my_seq_record):
        assert my_seq_record._seqrange == (1, 10)

    def test_seqrange_for_getitem(self, my_seq_record):
        """
        For MySeqRecords created from __getitem__() calls, determine the
        seqrange value by using the "parent's" _seqrange attribute
        """
        for ((start, end), seq, (seqstart, seqend)) in [
                ((2, 6),    'CDEF',   (3, 6)),
                ((0, 4),    'ABCD',   (1, 4)),
                ((4, None), 'EFGHIJ', (5, 10))]:
            test_record = my_seq_record[slice(start, end)]
            assert str(test_record.seq) == seq
            assert test_record._seqrange == (seqstart, seqend)

    def test_get_raw_pos(self, my_seq_record):
        for test in [-1, len(my_seq_record) + + 1]:
            with pytest.raises(ValueError):
                my_seq_record.get_raw_pos(test)
        for test, expected in [(1, 0), (3, 2), (10, 9)]:
            assert my_seq_record.get_raw_pos(test) == expected

    def test_get_log_pos(self, my_seq_record):
        for test in [len(my_seq_record), -len(my_seq_record) - 1]:
            with pytest.raises(IndexError):
                my_seq_record.get_log_pos(test)
        for test, expected in [(0, 1), (1, 2), (9, 10),
                               (-1, 10,), (-2, 9), (-10, 1)]:
            assert my_seq_record.get_log_pos(test) == expected

    def test_make_pir_description(self, my_seq_record):
        spec = 'structureX'
        pirdesc = spec + ':target:1:A:10:A::::'
        assert my_seq_record._make_pir_description(spec=spec) == pirdesc

    def test_find_gap_positions(self, my_seq_record):
        with pytest.raises(NotImplementedError):
            # Generator function, so doesn't raise when called
            # Therefore we put it in a list
            list(my_seq_record.find_gap_positions(seqrange=[2, 4]))
        assert list(my_seq_record.find_gap_positions()) == []

    def test_compute_seqrange_target(self, my_seq_record, my_seq_record_target):
        assert my_seq_record._seqrange == (1, 10)
        assert my_seq_record_target._seqrange == (11, 14)


class TestMySeqRecordWithGaps:
    def test_init_with_gaps(self, my_seq_record_with_gaps):
        assert my_seq_record_with_gaps._seqrange == (1, 10)

    def test_seqrange_for_getitem_with_gaps(self, my_seq_record_with_gaps):
        for ((start, end), seq, (seqstart, seqend)) in [
                ((1, 6),    'B-C-D',   (2, 4)),
                ((0, 4),    'AB-C',   (1, 3)),
                ((3, None), 'C-DEFGHIJ', (3, 10))]:
            test_record = my_seq_record_with_gaps[slice(start, end)]
            assert str(test_record.seq) == seq
            assert test_record._seqrange == (seqstart, seqend)

    def test_get_raw_pos_with_gaps(self, my_seq_record_with_gaps):
        for test in [-1, len(my_seq_record_with_gaps) + 1]:
            with pytest.raises(ValueError):
                my_seq_record_with_gaps.get_raw_pos(test)
        for test, expected in [(1, 0), (3, 3), (4, 5), (10, 11)]:
            assert my_seq_record_with_gaps.get_raw_pos(test) == expected

    def test_get_log_pos_with_gaps(self, my_seq_record_with_gaps):
        for test in [len(my_seq_record_with_gaps),
                     -len(my_seq_record_with_gaps) - 1]:
            with pytest.raises(IndexError):
                my_seq_record_with_gaps.get_log_pos(test)
        for test, expected in [(1, 2), (2, 2), (3, 3), (4, 3), (11, 10),
                               (-1, 10,), (-2, 9),
                               (-8, 3), (-9, 3), (-10, 2), (-11, 2), (-12, 1)]:
            assert my_seq_record_with_gaps.get_log_pos(test) == expected

    def test_find_gap_positions_with_gaps(self, my_seq_record_with_gaps,
                                                my_seq_record_with_gaps1,
                                                my_seq_record_with_gaps2):
        expected = {my_seq_record_with_gaps:  [(2, 2), (4, 3)],
                    my_seq_record_with_gaps1: [(0, 0), (1, 0), (4, 2), (6, 3)],
                    my_seq_record_with_gaps2: [(2, 2), (4, 3), (11, 9),
                                               (13, 10), (14, 10), (15, 10)]}
        for testrecord, expectedgaps in expected.iteritems():
            assert list(testrecord.find_gap_positions()) == expectedgaps

    def test_compute_seqrange_with_gaps(self, my_seq_record_target_with_gaps,
                                              my_seq_record_target_with_multiple_gaps):
        assert my_seq_record_target_with_gaps._seqrange == (11, 14)
        assert my_seq_record_target_with_multiple_gaps._seqrange == (11, 14)


class TestMyAln:
    def test_getitem_with_single_index(self, my_aln_simple):
        expected_seqs = ['BC-D', 'WXYZ']
        for i, expected_seq in enumerate(expected_seqs):
            assert isinstance(my_aln_simple[i], mkaln.MySeqRecord)
            assert my_aln_simple[i].seq.tostring() == expected_seq

    def test_getitem_as_iterator(self, my_aln_simple):
        expected_seqs = ['BC-D', 'WXYZ']
        for test_seqrec, expected_seq in zip(my_aln_simple, expected_seqs):
            assert test_seqrec.seq.tostring() == expected_seq

    def test_getitem_with_slice(self, my_aln_simple):
        """
        For now, this only tests if the returned alignment is actually
        an instance of MyAln.
        """
        expected_seqs = ['BC-D', 'WXYZ']
        test_aln = my_aln_simple[:]
        assert isinstance(test_aln, mkaln.MyAln)
        for test_seqrec, expected_seq in zip(test_aln, expected_seqs):
            assert test_seqrec.seq.tostring() == expected_seq

    def test_append_gap_at_1(self, my_aln_full_target, my_aln_template_gap_at_1):
        my_aln_template_gap_at_1.update_seqranges()
        my_aln_full_target.append(my_aln_template_gap_at_1[1], pad=True)
        expected = '-W-XY-----'
        assert my_aln_full_target[1].seq.tostring() == expected

    def test_append_2_gaps_at_1(self, my_aln_full_target, my_aln_template_2_gaps_at_1):
        my_aln_template_2_gaps_at_1.update_seqranges()
        my_aln_full_target.append(my_aln_template_2_gaps_at_1[1], pad=True)
        expected = '-W--X-----'
        assert my_aln_full_target[1].seq.tostring() == expected

    def test_append_gap_at_1(self, my_aln_full_target, my_aln_template_gap_at_1, my_aln_template_2_gaps_at_1):
        my_aln_template_gap_at_1.update_seqranges()
        my_aln_full_target.append(my_aln_template_gap_at_1[1], pad=True)
        my_aln_template_2_gaps_at_1.update_seqranges()
        my_aln_full_target.append(my_aln_template_2_gaps_at_1[1], pad=True)
        expected_1_gap  = '-W-XY-----'
        expected_2_gaps = '-W--X-----'
        assert my_aln_full_target[1].seq.tostring() == expected_1_gap
        assert my_aln_full_target[2].seq.tostring() == expected_2_gaps

    def test_append_full_range(self, my_aln_full_target, my_aln_full_range):
        my_aln_full_range.update_seqranges()
        my_aln_full_target.append(my_aln_full_range[1], pad=True)
        expected = 'W---X-Y--Z'
        assert my_aln_full_target[1].seq.tostring() == expected

    def test_append_1_gap_at_start(self, my_aln_full_target, my_aln_1_gap_at_start):
        my_aln_1_gap_at_start.update_seqranges()
        my_aln_full_target.append(my_aln_1_gap_at_start[1], pad=True)
        expected = '-W--X-Y--Z'
        assert my_aln_full_target[1].seq.tostring() == expected

    def test_append_1_gap_at_end(self, my_aln_full_target, my_aln_1_gap_at_end):
        my_aln_1_gap_at_end.update_seqranges()
        my_aln_full_target.append(my_aln_1_gap_at_end[1], pad=True)
        expected = 'W--X-Y--Z-'
        assert my_aln_full_target[1].seq.tostring() == expected

    def test_update_seqranges(self, my_aln_simple):
        assert my_aln_simple.update_seqranges() == (2, 4)
        assert my_aln_simple[1]._seqrange == (2, 4)
