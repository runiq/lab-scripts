#!/usr/bin/env python2

import textwrap

import Bio.Align
import Bio.SeqIO
import Bio.SeqRecord
import Bio.Alphabet


class MySeqRecord(object):
    """
    A wrapper for SeqRecords that augments them with some new methods.
    """
    def __init__(self, base, spec='structureX', nogaps=False, seqrange=None):
        # Account for filenames instead of SeqRecords
        if isinstance(base, str):
            base = Bio.SeqIO.read(base, 'fasta',
                                  alphabet=Bio.Alphabet.IUPAC.protein)
        self._base = base
        if seqrange is not None:
            self._seqrange = seqrange
        else:
            if nogaps is True:
                self._seqrange = (1, len(self))
            else:
                self._compute_seqrange()
        self._pir_description = self._make_pir_description(spec=spec)

    def __getattr__(self, name):
        return getattr(self._base, name)

    def __setattr__(self, name, value):
        if name is '_base':
            super(MySeqRecord, self).__setattr__(name, value)
        else:
            setattr(self._base, name, value)

    def __delattr__(self, name):
        if hasattr(self._base, name):
            self._base.__delattr__(name)
        else:
            super(MySeqRecord, self).__delattr__(name)

    def __getitem__(self, index):
        if isinstance(index, slice):
            # "None" slices are from the very start/to the very end
            raw_start = 0 if index.start is None else index.start
            raw_end = -1 if index.stop is None else index.stop - 1
            seqrange = (self.get_log_pos(raw_start), self.get_log_pos(raw_end))

            return MySeqRecord(self._base.__getitem__(index),
                               seqrange=seqrange)
        else:
            return self._base.__getitem__(index)

    def __iter__(self):
        return self._base.__iter__()

    def __contains__(self, char):
        return self._base.__contains__(char)

    def __len__(self):
        return self._base.__len__()

    def __bool__(self):
        return self._base.__bool__(self)

    def __add__(self, other):
        return MySeqRecord(self._base.__add__(other))

    def __radd__(self, other):
        return MySeqRecord(self._base.__radd__(other))

    def _compute_seqrange(self):
        try:
            seqrange_start = int(self.description.split()[1])
        except (IndexError):
            seqrange_start = 1
        log_seqlen = len(self._base) - str(self._base.seq).count('-')
        seqrange_end = seqrange_start + log_seqlen - 1
        self._seqrange = (seqrange_start, seqrange_end)

    def get_raw_pos(self, log_pos):
        """
        For a logical position, returns the raw position in the
        underlying string.
        """
        if log_pos not in xrange(self._seqrange[0] - 1, self._seqrange[1] + 1):
            raise ValueError("Position " + str(log_pos) +
                             " outside of sequence range")
        cur_pos = self._seqrange[0] - 1
        if log_pos == cur_pos:
            return cur_pos
        for i, res in enumerate(self.seq, start=0):
            if res is not '-':
                cur_pos += 1
            if cur_pos == log_pos:
                return i

    def get_log_pos(self, raw_pos):
        """
        For a given raw position, get the logical position.
        """
        if raw_pos < -len(self) or raw_pos >= len(self):
            raise IndexError(str(raw_pos) + " outside of sequence range")
        if raw_pos == 0:
            return self._seqrange[0]
        seqstart = self._seqrange[0]
        # Find number of gaps until raw_pos
        if raw_pos == -1:
            subseq = self.seq[:]
        else:
            subseq = self.seq[:raw_pos + 1]
        numgaps = str(subseq).count('-')
        return seqstart + len(subseq) - numgaps - 1

    def _make_pir_description(self, spec='structureX'):
        """
        Adds a MODELLER-conforming description attribute.
        """
        filename = self.name
        res_from, res_to = (str(i) for i in self._seqrange)
        chain_from = chain_to = 'A'
        # The last few fields are empty
        pir_description = ':'.join([spec, filename, res_from, chain_from,
                                    res_to, chain_to, ':::'])
        return pir_description

    def find_gap_positions(self, seqrange=None):
        """
        Yields gap positions. Example: A sequence "--AB-C--DE" would
        return
        iter([(0, 0), (1, 0), (4, 2), (6, 3), (7, 3)]).
        """
        if seqrange is not None:
            raise NotImplementedError("seqrange argument not yet implemented")
        pos = self._seqrange[0] - 1
        for i, res in enumerate(self):
            if res is '-' and (seqrange is None or pos in xrange(*seqrange)):
                yield (i, pos)
            if res is not '-':
                pos += 1

    def insert_gap(self, target_pos, num_gaps=1, raw=False):
        """
        Inserts one or more gaps into a SeqRecord at a predetermined
        position, and returns a tuple of (raw_position,
        sequence_position).

        If target_raw is True, the target position is used as an index
        in the *string* of the underlying sequence, not as a sequence
        position.
        """
        s = self.seq.tomutable()
        cur_pos = self._seqrange[0] - 1
        for i, res in enumerate(self.seq, start=(0 if raw else cur_pos)):
            metric = i if raw else cur_pos
            if metric == target_pos:
                for _ in range(num_gaps):
                    s.insert(i + 1, '-')
                self.seq = s.toseq()
                return (i, cur_pos)
            if res is not '-':
                cur_pos += 1
        else:
            # Gap wasn't inserted in the sequence so it should be
            # appended instead
            s.append('-')
            self.seq = s.toseq()
            return (len(self.seq) - 1, cur_pos)


class MyAln(Bio.Align.MultipleSeqAlignment):
    """
    Basically just a container for our stack.
    """
    def __getitem__(self, index):
        if isinstance(index, int):
            return self._records[index]
        elif isinstance(index, slice):
            return MyAln((MySeqRecord(record) for record in
                          self._records[index]), self._alphabet)
        elif len(index) != 2:
            raise TypeError("Invalid index type.")

        # Double indexing, baby
        row_index, col_index = index
        if isinstance(row_index, int):
            # Return (a part of) a single SeqRecord
            return MySeqRecord(super(MyAln, self).__getitem__(index))
        elif isinstance(col_index, int):
            # Return a string with the sequence in a given column
            return super(MyAln, self).__getitem__(index)
        else:
            # Return a "window" of the alignment
            return MyAln((rec[col_index] for rec in
                          self._records[row_index]), self._alphabet)

    def append(self, seqrec, pad=False):
        """
        Wrapper around the MyAln._append() function. Basically
        Bio.Align.MultipleSequenceAlignment.append(), but accounts for
        padding with gaps.
        """
        if self._records:
            self._append(seqrec, self.get_alignment_length(), pad=pad)
        else:
            self._append(seqrec, pad=pad)

    def _append(self, seqrec, expected_length=None, pad=False):
        """
        A reimplementation of MultipleSequenceAlignment's _append
        method.
        Checks for MySeqRecords in addition to the normal SeqRecord, and
        pads sequences if pad is True.
        """
        if not isinstance(seqrec, MySeqRecord):
            if isinstance(seqrec, Bio.SeqRecord.SeqRecord):
                seqrec = MySeqRecord(seqrec)
            else:
                raise TypeError("New sequence is not a MySeqRecord or "
                                "SeqRecord object")

        if (expected_length is not None and len(seqrec) != expected_length):
            if pad:
                seqrec_start = self[0].get_raw_pos(seqrec._seqrange[0])
                seqrec_end = self[0].get_raw_pos(seqrec._seqrange[1])
                gaps_front = '-' * (seqrec_start)
                gaps_aft = '-' * (expected_length - seqrec_end - 1)
                seqrec = gaps_front + seqrec + gaps_aft
            else:
                raise ValueError("Sequences must all be the same length")

        if not Bio.Alphabet._check_type_compatible([self._alphabet,
                                                    seqrec.seq.alphabet]):
            raise ValueError("New sequence's alphabet is incompatible")
        self._records.append(seqrec)

    def add_gap(self, position, num_gaps=1, raw=False):
        """
        Adds a gap at a logical position in the alignment.
        """
        # Add to target, get raw position
        raw_pos, _ = self[0].insert_gap(position, raw=raw)
        for seqrec in self[1:]:
            seqrec.insert_gap(position, num_gaps=num_gaps, raw=True)

    def write_pir(self, outfh):
        for seqrec in self:
            try:
                outfh.write('>P1;' + seqrec.filename + '\n')
            except AttributeError:
                outfh.write('>P1;' + seqrec.name + '\n')
            outfh.write(seqrec._pir_description + '\n')
            for line in textwrap.wrap(seqrec.seq.tostring()):
                outfh.write(line + '\n')
            outfh.write('*\n')

    def fill_new_gaps(self, seqrec):
        """
        Compares the alignment with another seqrec and adds only
        those gap positions that are not in the alignment's first
        sequence.

        The other seqrec MUST ONLY have ADDITIONAL gaps and is not
        altered. If there is a gap in the current seqrec, an exception
        is thrown.
        """
        # The first sequence is the target and determines our examined
        # (logical) sequence range
        if '-' not in self[0]:
            return
        test_range = self[0]._seqrange
        # We use this sequence range to get the equivalent raw positions
        # from the stack's target sequence
        start = seqrec.get_raw_pos(test_range[0])
        stop = seqrec.get_raw_pos(test_range[1] + 1)
        seqrec_trunc = seqrec[start:stop]
        seqrec_trunc._seqrange = test_range
        while str(self[0].seq) != str(seqrec_trunc.seq):
            for (i, (cur, oth)) in enumerate(zip(self[0], seqrec_trunc)):
                if cur != oth:
                    self.add_gap(i, raw=True)
                    break
            if len(self[0]) > len(seqrec_trunc):
                raise ValueError("Something went wrong, the new sequence "
                                 "has somehow gotten longer than the old one.")

    def update_seqranges(self):
        """
        Copies the seqrange from the first sequence to all the other
        sequences in the alignment.
        """
        seqrange = self[0]._seqrange
        for i in self[1:]:
            i._seqrange = seqrange
        return seqrange


def parse_args():
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('-t', '--target', metavar='FILE', default="NF90.fasta",
                   help='Target FASTA file (default: NF90.fasta)')
    p.add_argument('-o', '--output', metavar='FILE', default="aln_out.pir",
                   help='Output PIR file (default: aln_out.pir)')
    p.add_argument('template', metavar='TEMPLATE', nargs='+',
                   help='Template FASTA files')
    p.add_argument('-S', '--skip-templates-with-gaps', action='store_true',
                   help='Skip files with gaps in templates')
    return p.parse_args()


def main():
    args = parse_args()
    # The base target is read in without gaps
    stack = MyAln([MySeqRecord(args.target, nogaps=True)])
    for filename in args.template:
        new = MyAln(MySeqRecord(seqrec) for seqrec in
                    Bio.SeqIO.parse(filename, 'fasta',
                                    alphabet=Bio.Alphabet.IUPAC.protein))
        new.update_seqranges()
        tar_new = new[0]
        if '-' in tar_new:
            if args.skip_templates_with_gaps:
                continue
            for _, log_pos in tar_new.find_gap_positions():
                stack.add_gap(log_pos)
        # Add gaps from existing ALN to new target & template sequences
        new.fill_new_gaps(stack[0])
        # Append new template to the existing alignment
        stack.append(new[1], pad=True)
        with open(args.output, 'w') as outfh:
            stack.write_pir(outfh)


if __name__ == '__main__':
    main()
