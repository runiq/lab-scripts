#!/usr/bin/env python2

# TODO:
# - Write JalView features file for an alignment of all specified
#   sequences: http://www.jalview.org/help/html/features/featuresFormat.html

import argparse as ap
from collections import OrderedDict
from itertools import izip
import re
import HTMLParser

import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt


def parse_rampage(rampage_fn):
    """
    Parses a Rampage results .html file. Returns a tuple of the form
    (filename, rama_classes, residues).
    """
    def get_filename(text):
        """
        Returns the filename of the PDB file that was checked.
        """
        filenameregex = re.compile(r'<p>File: (.*)<br><br>')
        match = re.search(filenameregex, text)
        # There might be stuff that needs to be un-HTML-ified
        h = HTMLParser.HTMLParser()
        return h.unescape(match.group(1))

    def get_rama_classes(text):
        """
        Returns a dictionary with absolute and relative number of residues
        in favoured/allowed/outlier classes.

        The first value in the tuple is the absolute value, the second value
        is the relative value in per cent.
        """
        numsregex = re.compile(r'Number of residues in (favoured|allowed|outlier) region.* : +(\d+) \( *\d{1,3}\.\d%\)')
        fao = tuple(reversed([int(match.group(2)) for match in re.finditer(numsregex,
                text)]))
        return fao

    def get_residues(text):
        """
        Returns a list of tuples of form (chain, resnum, resname,
        rama_class) with those residues in allowed/outlier regions.
        """
        resregex = re.compile(r'Residue \[([A-Z ]) *(\d+) *:([A-Z]{3})\].*(Allowed|Outlier)')
        # DataFrames are not lazy as the size of the index must be known
        # at the time of construction
        matches = np.array(re.findall(resregex, text), dtype=[('chain', 'S1'), ('resid', 'i2'), ('resname', 'S3'), ('class', 'S7')])
        residues = pd.DataFrame(matches)

        # residues = pd.DataFrame(matches)
        return residues

    with open(rampage_fn) as rampage_fh:
        text = rampage_fh.read()

    fn = get_filename(text) or "None"
    rama_classes = get_rama_classes(text)
    residues = get_residues(text)
    return (fn, rama_classes, residues)


def write_texshade_file(residues, output, colors):
    def identify_chunks(residues):
        chunks = []
        itrws = residues.iterrows()
        # Catch a decoy with no non-favored residues
        # (That's a first)
        try:
            start = end = itrws.next()[1]['resid']
        except StopIteration:
            pass
        for _, record in itrws:
            if record['resid'] == end + 1:
                end = record['resid']
            else:
                chunks.append((start, end))
                start = end = record['resid']
        # Last chunk has to be appended manually
        chunks.append((start, end))
        return chunks

    def create_texshade_line(chunks, idx, fgcolor, bgcolor):
        # \shaderegion{idx}{...
        chunkline = '\\shaderegion{{{}}}{{'.format(idx)
        # ...chunks...
        chunkline += ','.join(('{}..{}'.format(start, end) for (start, end) in chunks))
        # ...}{fgcolor}{bgcolor}
        chunkline += '}}{{{fgcolor}}}{{{bgcolor}}}\n'.format(fgcolor=fgcolor, bgcolor=bgcolor)
        return chunkline

    with open(output, 'w') as output_fh:
        for idx, (name, ress) in enumerate(residues.iteritems(), start=1):
            allowed = identify_chunks(ress[ress['class'] == 'Allowed'])
            outliers = identify_chunks(ress[ress['class'] == 'Outlier'])
            for cls, fgcolor, bgcolor in [(allowed, colors['afg'], colors['abg']),
                    (outliers, colors['ofg'], colors['obg'])]:
                output_fh.write(create_texshade_line(cls, idx, fgcolor, bgcolor))


def plot_classes(data, output=None):
    d = pd.DataFrame.from_dict(data, orient='index')
    d.sort_index(inplace=True, ascending=False)
    d.columns = ['outlier', 'allowed', 'favored']

    f = plt.figure(dpi=300)
    f.set_figwidth(5.16)
    ax = f.add_subplot(111, xlabel='Residue index')
    d.plot(kind='barh', stacked=True, ax=ax)
    if output is None:
        f.show()
    else:
        f.savefig(output)


def write_csv(rama_classes, output, delimiter=','):
    with open(output, 'w') as fh:
        fh.write("#PDB file\toutlier\tallowed\tfavored\n")
        for (fn, (favored, allowed, outlier)) in rama_classes.iteritems():
            fh.write(delimiter.join([fn, str(favored), str(allowed), str(outlier)]) + '\n')


def parse_args():
    p = ap.ArgumentParser()
    p.add_argument('rampage_html', nargs='+', help='File to parse')
    p.add_argument('-o', '--output', default=None,
            help="Name of the chart file")
    p.add_argument('-n', '--names', nargs='+', default=None,
            help="Names of the models in the diagram")
    p.add_argument('-s', '--seqline', default=None,
            help="Write out a TeXshade \\shaderegion line for each sequence")
    p.add_argument('-c', '--csv', default=None,
            help="Write CSV file with data")
    p.add_argument('--delimiter', default='\t',
            help="Delimiter for CSV file")
    p.add_argument('--outlier-fgcolor', default='white')
    p.add_argument('--outlier-bgcolor', default='red')
    p.add_argument('--allowed-fgcolor', default='black')
    p.add_argument('--allowed-bgcolor', default='yellow')
    return p.parse_args()


def main():
    args = parse_args()
    if args.names is not None and len(args.rampage_html) != len(args.names):
        import sys
        print "Error: Length of names list must match number of supplied html files."
        print "Aborting."
        sys.exit(1)
    classes = {}
    ress = OrderedDict()
    colors = dict(afg=args.allowed_fgcolor, abg=args.allowed_bgcolor,
            ofg=args.outlier_fgcolor, obg=args.outlier_bgcolor)
    if args.names is None:
        for fn in args.rampage_html:
            pdb_fn, rama_classes, residues = parse_rampage(fn)
            classes[pdb_fn] = rama_classes
            ress[pdb_fn] = residues
    else:
        for name, fn in izip(args.names, args.rampage_html):
            _, rama_classes, residues = parse_rampage(fn)
            classes[name] = rama_classes
            ress[name] = residues
    # plot_classes(classes, output=args.output)
    if args.seqline is not None:
        write_texshade_file(ress, output=args.seqline, colors=colors)

    if args.csv is not None:
        write_csv(classes, output=args.csv, delimiter=args.delimiter)


if __name__  == '__main__':
    main()
